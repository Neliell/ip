package ro.unibuc.ro.IP.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.client.RestTemplate;
import ro.unibuc.ro.IP.controllers.RecipeController;
import ro.unibuc.ro.IP.repository.RecipeRepository;

@RunWith(MockitoJUnitRunner.class)
public class RecipeControllerTest {

    @Spy
    @InjectMocks
    private RecipeController recipeController;

    @Mock
    private RecipeRepository recipeRepository;

    @InjectMocks
    private RestTemplate restTemplate;

    @Before
    public void setUp() {

    }

    @Test
    public void testAllRecipes() {
        //TODO: Create tests

    }
}
