package ro.unibuc.ro.IP.util;

import com.google.common.collect.ImmutableMap;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;

@Component
public class RecipeFilters {

    public static HashMap<String, String> filters;

    @PostConstruct
    public void setDefault() {
        filters = new HashMap<>();
        filters.put("tipMasa", "");
        filters.put("dificultate", "");
    }

}
