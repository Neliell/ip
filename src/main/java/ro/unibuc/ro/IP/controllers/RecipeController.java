package ro.unibuc.ro.IP.controllers;

import org.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.unibuc.ro.IP.domain.Reteta;
import ro.unibuc.ro.IP.service.RecipeService;
import ro.unibuc.ro.IP.trait.Loggable;
import ro.unibuc.ro.IP.util.RecipeFilters;

import java.util.List;
import java.util.stream.Collectors;

@RestController()
@RequestMapping(path = "/recipe")
@CrossOrigin(origins = "http://localhost:4200", allowedHeaders = "*")
public class RecipeController implements Loggable {

    @Autowired
    RecipeService recipeService;

    @PostMapping(path = "/cuisine/{cuisineName}",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Reteta>> getRecipesByCuisine(@RequestBody String jsonRecipes, @PathVariable("cuisineName") String cuisineName) {

        JSONArray jsonArray = new JSONArray(jsonRecipes);
        List<Object> recipesMap = jsonArray.toList();
        List<Reteta> recipes = recipeService.getRecipesFromMap(recipesMap);

        return ResponseEntity.ok().body(
                recipes.stream().
                        filter((reteta) -> reteta.getTip_bucatarie().equals(cuisineName)).
                        collect(Collectors.toList()));
    }

    @GetMapping(path = "/mealType/{mealType}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Reteta>> getRecipesByMealType(@PathVariable(value = "mealType") String mealType) {
        logger().info("Retrieving recipes by meal type {}", mealType);
        return ResponseEntity.ok().body(recipeService.getRecipesByTypeOfMeal(mealType));
    }

    @GetMapping(path = "/difficulty/{difficulty}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Reteta>> getRecipesByDifficulty(@PathVariable(value = "difficulty") String difficulty) {
        logger().info("Retrieving recipes by difficulty {}", difficulty);
        return ResponseEntity.ok().body(recipeService.getRecipesByDifficulty(difficulty));
    }

    @GetMapping(path = "/mealAndDifficulty/{mealType}/{difficulty}",
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Reteta>> getRecipesByMealTypeAndDifficulty(
            @PathVariable(value = "mealType") String mealType,
            @PathVariable(value = "difficulty") String difficulty) {
        logger().info("Retrieving recipes by meal Type and Difficulty");
        return ResponseEntity.ok().body(recipeService.getRecipesByMealTypeAndDifficulty(mealType, difficulty));
    }

    @GetMapping(path = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Reteta>> getAllRecipes() {
        logger().info("Retrieving all recipes");
        return ResponseEntity.ok().body(recipeService.getAllRecipes());
    }

    @GetMapping(path = "all/desc", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Reteta>> findRecipeByAprecieri() {
        logger().info("Retrieving all recipes by likes in descending order");
        return ResponseEntity.ok().body(recipeService.getRecipesByLikesDesc());
    }

    @RequestMapping(path = "/filters/mealType/{mealType}",
            method = {RequestMethod.GET, RequestMethod.POST},
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> setMealType(@PathVariable(value = "mealType") String mealType) {
        RecipeFilters.filters.put("tipMasa", mealType);
        return ResponseEntity.ok("Meal type set to: " + mealType);
    }

    @RequestMapping(path = "/filters/difficulty/{difficulty}",
            method = {RequestMethod.GET, RequestMethod.POST},
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> setDifficulty(@PathVariable(value = "difficulty") String difficulty) {
        RecipeFilters.filters.put("dificultate", difficulty);
        return ResponseEntity.ok("Difficulty set to: " + difficulty);
    }

    @GetMapping(path = "/filters", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getRecipeFilters() {
        return ResponseEntity.ok().body(RecipeFilters.filters.toString());
    }

    @GetMapping(path = "/prolog/{cuisine}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List> getRecipesByCuisineWithFilters(@PathVariable("cuisine") String cuisine) {
        return recipeService.getRecipesByCuisine(cuisine);
    }
}
