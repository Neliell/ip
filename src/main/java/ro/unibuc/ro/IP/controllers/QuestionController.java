package ro.unibuc.ro.IP.controllers;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ro.unibuc.ro.IP.domain.Answer;
import ro.unibuc.ro.IP.domain.PrologOutput;
import ro.unibuc.ro.IP.prolog.Prolog;

@RestController
@RequestMapping("/form")
@CrossOrigin(origins = "http://localhost:4200", allowedHeaders = "*")
public class QuestionController {

    private static Prolog prolog;

    private Prolog getProlog() throws Exception {
        if (prolog == null)
            prolog = new Prolog();

        return prolog;
    }

    @GetMapping(path = "/output", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PrologOutput getNextOutput() throws Exception {
        return getProlog().getPrologOutput();
    }

    @PostMapping(path = "/output", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PrologOutput getNextOutput(Answer answer) throws Exception {
        getProlog().addAnswer(answer.getAnswer());
        PrologOutput prologOutput;
        while ((prologOutput = getProlog().getPrologOutput()) == null) {

        }
        return prologOutput;
    }
}
