package ro.unibuc.ro.IP.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Mirela on 21.10.2018.
 */
@Getter
@Setter
public class Answer{
    String answer;
}
