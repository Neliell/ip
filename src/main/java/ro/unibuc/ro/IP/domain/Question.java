package ro.unibuc.ro.IP.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Question extends PrologOutput {
    private String questionName;
    private String answerOptions[];

    public Question(String questionName) {
        this.questionName = questionName;
    }
}
