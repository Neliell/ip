package ro.unibuc.ro.IP.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
public class Reteta {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "secventaReteta", sequenceName = "secventaReteta", initialValue = 1, allocationSize = 1)
    private Long id;
    private String nume_reteta;
    private String tip_bucatarie;
    private String tip_masa;
    @Column(columnDefinition = "TEXT")
    private String descriere_reteta;
    private String imagine_reteta;
    @Column(columnDefinition = "TEXT")
    private String ingrediente_reteta;
    @Column(columnDefinition = "TEXT")
    private String mod_preparare;
    private String timp_preparare;
    private String grad_dificultate;
    private String numar_portii;
    private Long aprecieri;

    public Reteta(String nume_reteta, String tip_bucatarie, String tip_masa, String descriere_reteta, String imagine_reteta, String ingrediente_reteta, String mod_preparare, String timp_preparare, String grad_dificultate, String numar_portii, Long aprecieri) {
        this.nume_reteta = nume_reteta;
        this.tip_bucatarie = tip_bucatarie;
        this.tip_masa = tip_masa;
        this.descriere_reteta = descriere_reteta;
        this.imagine_reteta = imagine_reteta;
        this.ingrediente_reteta = ingrediente_reteta;
        this.mod_preparare = mod_preparare;
        this.timp_preparare = timp_preparare;
        this.grad_dificultate = grad_dificultate;
        this.numar_portii = numar_portii;
        this.aprecieri = aprecieri;
    }

}
