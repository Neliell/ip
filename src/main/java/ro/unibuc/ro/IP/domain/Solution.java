package ro.unibuc.ro.IP.domain;

import lombok.AllArgsConstructor;

/**
 * Created by Mirela on 21.10.2018.
 */
@AllArgsConstructor
public class Solution extends PrologOutput {

    String solution;
    boolean isFirstSolution;
}
