package ro.unibuc.ro.IP.trait;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;

public interface Loggable {

    @Bean
    default Logger logger() {
        return LoggerFactory.getLogger(this.getClass());
    }
}
