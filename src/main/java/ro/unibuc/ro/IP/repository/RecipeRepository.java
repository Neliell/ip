package ro.unibuc.ro.IP.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ro.unibuc.ro.IP.domain.Reteta;

import java.util.List;


@Repository
@Transactional(readOnly = true)
public interface RecipeRepository extends JpaRepository<Reteta, Long> {

    @Query(value = "SELECT * FROM reteta WHERE tip_bucatarie = ?1", nativeQuery = true)
    List<Reteta> findByCuisine(String tipBucatarie);

    @Query(value = "SELECT * FROM reteta WHERE tip_masa = :mealType", nativeQuery = true)
    List<Reteta> findByMealType(@Param("mealType")String tipMasa);

    @Query(value = "SELECT * FROM reteta WHERE grad_dificultate = :dificultate", nativeQuery = true)
    List<Reteta> findByDifficulty(@Param("dificultate")String difficulty);

    @Query(value = "SELECT * FROM reteta WHERE tip_masa = :mealType AND grad_dificultate = :dificultate", nativeQuery = true)
    List<Reteta> findByMealTypeAndDifficulty(@Param("mealType")String tipMasa,@Param("dificultate")String difficulty);

    List<Reteta> findAll();

    @Query(value = "SELECT * FROM reteta WHERE nume_reteta = ?1", nativeQuery = true)
    Reteta findByName(String nume);

    @Query(value = "SELECT * FROM reteta ORDER BY aprecieri DESC", nativeQuery = true)
    List<Reteta> findRecipeByAprecieri();

}
