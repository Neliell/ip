package ro.unibuc.ro.IP.prolog;

import ro.unibuc.ro.IP.trait.Loggable;

import java.io.IOException;
import java.io.InputStream;
import java.io.PipedOutputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.util.concurrent.ConcurrentLinkedQueue;

public class ConexiuneProlog implements Loggable {

    private ExpeditorMesaje expeditor;
    private CititorMesaje cititor;
    private int port;

    ConexiuneProlog(int port) throws IOException, InterruptedException {
        String directorProiect = System.getProperty("user.dir");

        InputStream processIs, processStreamErr;
        this.port = port;
        ServerSocket serverSocket = new ServerSocket(port);
        //Socket sock_s=servs.accept();
        cititor = new CititorMesaje(this, serverSocket);
        cititor.start();
        expeditor = new ExpeditorMesaje(cititor);
        expeditor.start();

        Runtime runtime = Runtime.getRuntime();

        String caleExecutabilSicstus = directorProiect + "\\prolog\\bin\\spwin.exe";
        System.out.println("cale sicstus:" + caleExecutabilSicstus);
        String nume_fisier = "sistem.pl";
        String scop = "inceput.";
        String reguli = "reguli_carte.txt";

        String comanda = caleExecutabilSicstus + " -f -l " + nume_fisier + " --goal " + scop + " -a " + port;

        logger().info("Running spwin.exe");
        Process procesSicstus = runtime.exec(comanda);

        //InputStream-ul din care citim ce scrie procesul
        processIs = procesSicstus.getInputStream();
        //stream-ul de eroare
        processStreamErr = procesSicstus.getErrorStream();

        logger().info("Setting working directory");
        expeditor.trimiteMesajSicstus("director('" + directorProiect.replace("\\", "/") + "')");

        logger().info("Loading prolog rules from file:{}", reguli);
        expeditor.trimiteMesajSicstus("incarca('" + reguli + "')");

        expeditor.trimiteMesajSicstus("comanda(consulta)");
    }


    void opresteProlog() throws InterruptedException {
        PipedOutputStream pos = this.expeditor.getPipedOutputStream();
        PrintStream ps = new PrintStream(pos);
        ps.println("exit.");
        ps.flush();
    }

    public ExpeditorMesaje getExpeditor() {
        return expeditor;
    }

    public CititorMesaje getCititor() {
        return cititor;
    }
}
