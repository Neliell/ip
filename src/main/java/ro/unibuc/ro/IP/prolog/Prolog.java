package ro.unibuc.ro.IP.prolog;

import ro.unibuc.ro.IP.*;
import ro.unibuc.ro.IP.domain.PrologOutput;

import java.io.IOException;

/**
 * Created by Mirela on 21.10.2018.
 */

public class Prolog {

    private ConexiuneProlog prologConnection;

    public Prolog() throws IOException, InterruptedException {
        prologConnection = new ConexiuneProlog(5007);
    }

    public PrologOutput getPrologOutput() {
        return prologConnection.getCititor().getMessageQueue().poll();
    }

    public void addAnswer(String answer) throws Exception {
        prologConnection.getExpeditor().trimiteMesajSicstus(answer);
    }

}
