/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ro.unibuc.ro.IP.prolog;


import lombok.AllArgsConstructor;
import ro.unibuc.ro.IP.domain.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;

public class CititorMesaje extends Thread {

    private ServerSocket servs;
    private volatile Socket s = null;//volatile ca sa fie protejat la accesul concurent al mai multor threaduri
    private volatile PipedInputStream pis = null;
    private ConexiuneProlog conexiune;
    private ConcurrentLinkedQueue<PrologOutput> messageQueue = new ConcurrentLinkedQueue<PrologOutput>();

    //setteri sincronizati
    public synchronized void setSocket(Socket _s) {
        s = _s;
        notify();
    }

    private final synchronized void setPipedInputStream(PipedInputStream _pis) {
        pis = _pis;
        notify();
    }
    //getteri sincronizati

    public synchronized Socket getSocket() throws InterruptedException {
        if (s == null) {
            wait();//asteapta pana este setat un socket
        }
        return s;
    }

    public synchronized PipedInputStream getPipedInputStream() throws InterruptedException {
        if (pis == null) {
            wait();
        }
        return pis;
    }

    public ConcurrentLinkedQueue<PrologOutput> getMessageQueue() {
        return messageQueue;
    }

    CititorMesaje(ConexiuneProlog conexiuneProlog, ServerSocket serverSocket) {
        servs = serverSocket;
        conexiune = conexiuneProlog;
    }

    @Override
    public void run() {
        try {
            //apel blocant, asteapta conexiunea
            //conexiunea clinetului se face din prolog
            Socket s_aux = servs.accept();
            setSocket(s_aux);
            //pregatesc InputStream-ul pentru a citi de pe Socket
            InputStream is = s_aux.getInputStream();

            PipedOutputStream pos = new PipedOutputStream();
            setPipedInputStream(new PipedInputStream(pos, 10000000));//leg un pipedInputStream de capatul in care se scrie

            int chr;
            String str = "";
            Question lastQuestion = null;
            while ((chr = is.read()) != -1) {//pana nu citeste EOF
                pos.write(chr);//pun date in Pipe, primite de la Prolog
                str += (char) chr;
                if (chr == '\n') {
                    final String sirDeScris = str;
                    str = "";
                    String text = sirDeScris.trim();
                    System.out.println("Text citit e: " + text);
//                            if (text.length() > 3 && text.charAt(0) == 'd' && text.charAt(1) == '(' && text.charAt(text.length() - 1) == ')') {
//                                String intrebare = text.substring(2, text.length() - 1);
//                                String[] cuvinte = intrebare.split("#");
//
//                                try {
//                                    conexiune.getFereastra().setImagine(cuvinte[0], cuvinte[1]);
//                                } catch (IOException ex) {
//                                    Logger.getLogger(CititorMesaje.class.getName()).log(Level.SEVERE, null, ex);
//                                }
//                            }
//                            else
                    //verific daca e intrebare
                    if (text.length() > 2 && text.charAt(0) == 'i' && text.charAt(1) == '(' && text.charAt(text.length() - 1) == ')') {
                        String intrebare = text.substring(2, text.length() - 1);
                        lastQuestion = new Question(intrebare);
                        messageQueue.add(lastQuestion);
                    } //verific daca sunt optiuni
                    else if (text.length() > 2 && text.charAt(0) == '(' && text.charAt(text.length() - 1) == ')') {
                        System.out.println("Am intrat in setOptiuni. ");

                        String options = text.substring(1, text.length() - 1);
                        lastQuestion.setAnswerOptions(options.split("#"));
                    }//verific daca este meniu secundar
                    else if (text.length() > 2 && text.charAt(0) == 'm' && text.charAt(1) == '(' && text.charAt(text.length() - 1) == ')') {
                        System.out.println("Meniu secundar!!!");
                    }
                    //verific daca e prima solutie
                    else if (text.length() > 2 && text.charAt(0) == 'k' && text.charAt(1) == '(' && text.charAt(text.length() - 1) == ')') {
                        System.out.println("Am primit solutia: " + text);
                        String solutie = text.substring(2, text.length() - 1);
                        messageQueue.add(new Solution(solutie, true));
                    }
                    //verific daca e solutie
                    else if (text.length() > 2 && text.charAt(0) == 's' && text.charAt(1) == '(' && text.charAt(text.length() - 1) == ')') {
                        System.out.println("Am primit solutia: " + text);
                        String solutie = text.substring(2, text.length() - 1);
                        messageQueue.add(new Solution(solutie, false));
                    }
                    //verific daca e primul fapt/nu exista fapte
                    else if (text.length() > 2 && text.charAt(0) == 'n' && text.charAt(1) == '(' && text.charAt(text.length() - 1) == ')') {
                        System.out.println("Am primit primul fapt: " + text);
                        String fapt = text.substring(2, text.length() - 1);
//                        messageQueue.add(new Fact(fapt, true));
                    }
                    //verific daca e un fapt
                    else if (text.length() > 2 && text.charAt(0) == 'f' && text.charAt(1) == '(' && text.charAt(text.length() - 1) == ')') {
                        System.out.println("Am primit primul fapt: " + text);
                        String fapt = text.substring(2, text.length() - 1);
//                        messageQueue.add(new Fact(fapt, false));
                    }
                }
            }

        } catch (IOException ex) {
            Logger.getLogger(CititorMesaje.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


}
