package ro.unibuc.ro.IP.prolog;

import java.io.*;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ExpeditorMesaje extends Thread {
    private CititorMesaje cititorMesaje;
    private volatile PipedOutputStream pipedOutputStream = null;
    private volatile PipedInputStream pipedInputStream;
    //    volatile boolean gata = false;

    //setteri sincronizati
    public final synchronized void setPipedOutputStream(PipedOutputStream pipedOutputStream) {
        this.pipedOutputStream = pipedOutputStream;
        notify();
    }

    //getteri sincronizati
    public synchronized PipedOutputStream getPipedOutputStream() throws InterruptedException {
        if (pipedOutputStream == null) {
            wait();
        }
        return pipedOutputStream;
    }


    ExpeditorMesaje(CititorMesaje cititorMesaje) throws IOException {
        this.cititorMesaje = cititorMesaje;
        pipedInputStream = new PipedInputStream();
        setPipedOutputStream(new PipedOutputStream(pipedInputStream));
    }

    public void trimiteMesajSicstus(String mesaj) throws InterruptedException {
        PipedOutputStream pos = getPipedOutputStream();
        PrintStream ps = new PrintStream(pos);
        ps.println(mesaj + ".");
        ps.flush();
    }
    public void trimiteDirectorSicstus(String mesaj) throws InterruptedException {
        PipedOutputStream pos = getPipedOutputStream();
        PrintStream ps = new PrintStream(pos);
        ps.println(mesaj);
        ps.flush();
    }

    public void trimiteSirSicstus(String mesaj) throws InterruptedException {
        PipedOutputStream pos = getPipedOutputStream();
        PrintStream ps = new PrintStream(pos);
        ps.println(mesaj);
        ps.flush();
    }

    public void run() {
        try {
            Socket socket = cititorMesaje.getSocket();
            OutputStream outputStream = socket.getOutputStream();
            int chr;
            while ((chr = pipedInputStream.read()) != -1) {
                outputStream.write(chr);
            }
            System.out.println("gata");


        } catch (IOException | InterruptedException ex) {
            Logger.getLogger(ExpeditorMesaje.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

