package ro.unibuc.ro.IP.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ro.unibuc.ro.IP.domain.Reteta;
import ro.unibuc.ro.IP.repository.RecipeRepository;
import ro.unibuc.ro.IP.util.RecipeFilters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Service
public class RecipeService {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private RecipeRepository recipeRepository;

    public ResponseEntity<List> getRecipesByCuisine(String cuisine) {
        HashMap<String, String> filters = RecipeFilters.filters;
        String mealType = filters.get("tipMasa");
        String difficulty = filters.get("dificultate");
        List recipes;

        String recipesByDifficultyUrl = "http://localhost:9092/recipe/difficulty/";
        String recipesByMealTypeUrl = "http://localhost:9092/recipe/mealType/";
        String recipesByMealTypeAndDifficultyUrl = "http://localhost:9092/recipe/mealAndDifficulty/";
        String allRecipesUrl = "http://localhost:9092/recipe/all";
        if (!mealType.equals("") && !difficulty.equals(""))
            recipes = restTemplate.getForEntity(String.format("%s%s/%s",
                    recipesByMealTypeAndDifficultyUrl, mealType, difficulty), List.class).getBody();
        else if (!mealType.equals(""))
            recipes = restTemplate.getForEntity(String.format("%s%s",
                    recipesByDifficultyUrl, difficulty), List.class).getBody();
        else if (!difficulty.equals(""))
            recipes = restTemplate.getForEntity(String.format("%s%s",
                    recipesByMealTypeUrl, mealType), List.class).getBody();
        else
            recipes = restTemplate.getForEntity(allRecipesUrl, List.class).getBody();


        String cuisineUrl = "http://localhost:9092/recipe/cuisine/";
        return restTemplate.exchange(cuisineUrl + cuisine, HttpMethod.POST,
                getEntity(recipes), List.class);
    }

    private HttpEntity<List> getEntity(List recipes) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        return new HttpEntity<>(recipes, headers);
    }

    public List<Reteta> getRecipesByTypeOfMeal(String typeOfMeal) {
        return recipeRepository.findByMealType(typeOfMeal);
    }

    public List<Reteta> getAllRecipes() {
        return recipeRepository.findAll();
    }

    public List<Reteta> getRecipesByDifficulty(String difficulty) {
        return recipeRepository.findByDifficulty(difficulty);
    }

    public List<Reteta> getRecipesByMealTypeAndDifficulty(String mealType, String difficulty) {
        return recipeRepository.findByMealTypeAndDifficulty(mealType, difficulty);
    }

    public List<Reteta> getRecipesByLikesDesc() {
        return recipeRepository.findRecipeByAprecieri();
    }

    public List<Reteta> getRecipesFromMap(List<Object> mapList) {
        List<Reteta> recipes = new ArrayList<>();

        for (Object mapRecipe : mapList) {
            HashMap hashMap = (HashMap) mapRecipe;
            String nume_reteta = (String) hashMap.get("nume_reteta");
            String tip_bucatarie = (String) hashMap.get("tip_bucatarie");
            String tip_masa = (String) hashMap.get("tip_masa");
            String descriere_reteta = (String) hashMap.get("descriere_reteta");
            String imagine_reteta = (String) hashMap.get("imagine_reteta");
            String ingrediente_reteta = (String) hashMap.get("ingrediente_reteta");
            String timp_preparare = (String) hashMap.get("timp_preparare");
            Integer aprecieri = (Integer) hashMap.get("aprecieri");
            Integer id = (Integer) hashMap.get("id");
            String mod_preparare = (String) hashMap.get("mod_preparare");
            String numar_portii = (String) hashMap.get("numar_portii");
            String grad_dificultate = (String) hashMap.get("grad_dificultate");

            Reteta recipe = new Reteta(new Long(id), nume_reteta, tip_bucatarie, tip_masa, descriere_reteta, imagine_reteta,
                    ingrediente_reteta, mod_preparare, timp_preparare, grad_dificultate, numar_portii, new Long(aprecieri));
            recipes.add(recipe);
        }
        return recipes;
    }

}
