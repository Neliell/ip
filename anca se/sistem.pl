/*
cod preluat din cartea(bibliografie[1]):
BALCAN Maria Florina, HRISTEA Florentina, 
Aspecte ale Cautarii si Reprezentarii Cunostintelor in Inteligenta Artificiala,
Editura Universitatii din Bucuresti, 2004, 
pg 216
*/


close_all:-current_stream(_,_,S),close(S),fail;true.
curata_bc:-current_predicate(P), abolish(P,[force(true)]), fail;true.

:-use_module(library(lists)).
:-use_module(library(file_systems)).
:-use_module(library(system)).
:-use_module(library(sockets)).

:-op(900,fy,not).

:-dynamic fapt/3.
:-dynamic interogat/1.
:-dynamic scop/1.
:-dynamic interogabil/3.
:-dynamic regula/3.

tab(N):- N>0,write(' '),N1 is N-1, tab(N1).
tab(0).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
tab(Stream,N):-N>0,write(Stream,' '),N1 is N-1, tab(Stream,N1).
tab(_,0).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

not(P):-P,!,fail.
not(_).

scrie_lista([]):-nl.
scrie_lista([H|T]) :-
	write(H), tab(1),
	scrie_lista(T).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
scrie_lista(Stream,[]):-nl(Stream),flush_output(Stream).

scrie_lista(Stream,[H|T]) :-
	write(Stream,H), tab(Stream,1),
	scrie_lista(Stream,T).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  

afiseaza_fapte :-
	write('Fapte existente în baza de cunostinte:'),
	nl,nl, write(' (Atribut,valoare) '), nl,nl,
	listeaza_fapte,nl.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Start
afiseaza_fapte(Stream) :- 
	fapt(av(Atr,Val),FC,_) ->
		(write(Stream,'n(Fapte existente in baza de cunostinte: )'),
		 nl(Stream), flush_output(Stream),
		 listeaza_fapte(Stream),nl(Stream)); 
		(nl(Stream), write(Stream, 'n(Nu exista fapte in baza de cunostinte!)')), 
		 nl(Stream), flush_output(Stream).	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End

listeaza_fapte:-  
	fapt(av(Atr,Val),FC,_), 
	write('('),write(Atr),write(','),
	write(Val), write(')'),
	write(','), write(' certitudine '),
	FC1 is integer(FC),write(FC1),
	nl,fail.
listeaza_fapte.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Start
/*listeaza_fapte(Stream):-  
	fapt(av(Atr,Val),FC,_), 
	write(Stream,'('),write(Stream, Atr),write(Stream,','),
	write(Stream, Val), write(Stream, ')'),
	write(Stream,','), write(Stream,' certitudine '),
	FC1 is integer(FC),write(Stream, FC1),
	nl(Stream), flush_output(Stream),fail.
listeaza_fapte(Stream).*/

listeaza_fapte(Stream):-  
	fapt(av(Atr,Val),FC,_), 
	FC1 is integer(FC),
	format(Stream,'f(~s#~s#~d)',[Atr,Val,FC1]),
	nl(Stream), flush_output(Stream), fail.
listeaza_fapte(Stream).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End

lista_float_int([],[]).
lista_float_int([Regula|Reguli],[Regula1|Reguli1]):-
	(Regula \== utiliz,
	Regula1 is integer(Regula);
	Regula ==utiliz, Regula1=Regula),
	lista_float_int(Reguli,Reguli1).

goleste_director([]).
goleste_director([H - _|T]):- 
	atom_concat('demonstratii/', H, Aux), 
	delete_file(Aux), 
	goleste_director(T).

pornire:-
	write('Pornire'), nl, 
	(directory_exists('demonstratii') -> (file_members_of_directory('demonstratii', L), goleste_director(L));
										 make_directory('demonstratii')),				 
	retractall(interogat(_)), 
	retractall(fapt(_,_,_)),  
	repeat,
		write('Introduceti una din urmatoarele optiuni: '), nl, nl,
		write(' (Incarca Consulta Reinitiaza  Afisare_fapte  Cum   Iesire) '),nl, nl,
		write('|: '),citeste_linie([H|T]), % citeste pana la enter
		executa([H|T]), H == iesire.  % repeta pana cand primeste iesire.

meniu_secundar(L) :-
	repeat,
		write('Reafiseaza Sumar? '), nl, nl,
		write(' (Reafiseaza_sumar Revenire) '),
		nl,nl,write('|: '),citeste_linie([H|T]), % citeste pana la enter
		executa([H|T],L), H == revenire.  % repeta pana cand primeste iesire.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Start
meniu_secundar(Stream,L):- 
	write('Am intrat in meniu secundar'),nl,
	write(Stream,'m(Reafiseaza_sumar#Revenire)'),
	nl(Stream), flush_output(Stream),
	repeat,
	write('repeat in meniu secundar'),nl,
	citeste_linie(Stream,[H|T]),
	write('Am citit'),nl,write(H),nl,write(T),nl,
	executa(Stream, [H|T], L), H == revenire.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End

executa([incarca]) :- 
	incarca,!,nl,
	write('Fisierul dorit a fost incarcat'),nl.

executa([consulta]) :- scopuri_princ,!.
executa([reinitiaza]) :- retractall(interogat(_)), retractall(fapt(_,_,_)),!.
executa([afisare_fapte]) :- afiseaza_fapte,!.
executa([cum|L]) :- cum(L),!.
executa([iesire]):-!.
executa([revenire],_):-!.
executa([reafiseaza_sumar],L):- afis_list_scop_scurt(L).

executa(Stream,[reafiseaza_sumar],L):- afis_list_scop_scurt(Stream,L).
executa(Stream,[revenire],_):-!.
executa(Stream, [afisare_fapte]) :- write('Afisez faptele in interfata'), nl, afiseaza_fapte(Stream),!.

executa([_|_]) :-
	write('Comanda incorecta! '),nl.

scopuri_princ :- 
	scop(Atr),write('Atributul este '),write(Atr),nl, 
	setof(st(FC,fapt(av(Atr,Val))), X^(determina(Atr), fapt(av(Atr,Val),FC,X), FC>=40),L)
	-> (write('Sunt solutii!'),nl, 
		afis_list_scop(L),
		meniu_secundar(L))
		; write('Nu exista solutii!'), nl.	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Start
scopuri_princ(Stream) :-
	scop(Atr),write('Atributul este '),write(Atr),nl, 
	setof(st(FC,fapt(av(Atr,Val))), 
	X^(determina(Stream,Atr), fapt(av(Atr,Val),FC,X), FC>=40),
	L)
	-> (write(Stream, 'k(Sunt urmatoarele solutii:)'), nl(Stream), flush_output(Stream), write(L),nl,
		afis_list_scop(Stream,L), 
		meniu_secundar(Stream,L)
		)
	; write(Stream,'k(Nu exista solutii!)'), nl(Stream), flush_output(Stream).	
	
scopuri_princ(_).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End

%****************************************************************
afis_list_sol_detaliat([H]):- H =.. [_, FC, Fapt],
					   Fapt =.. [_, Av],
					   Av =.. [_,Atr, Val], 
					   scrie_solutie_in_fisier(Av, FC),
					   scrie_scop(av(Atr, Val), FC), nl.	
						  
afis_list_sol_detaliat([H|T]):- afis_list_sol_detaliat(T), 
					   H =.. [_, FC, Fapt],
					   Fapt =.. [_, Av],
					   Av =.. [_,Atr, Val], 
					   scrie_solutie_in_fisier(Av, FC),
					   scrie_scop(av(Atr, Val), FC), nl.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Start
afis_list_sol_detaliat(Stream,[H]):- 
		H =.. [_, FC, Fapt],
		Fapt =.. [_, Av],
		Av =.. [_,Atr, Val], 
		scrie_solutie_in_fisier(Av, FC),
		scrie_scop(Stream,av(Atr, Val), FC).
		   
afis_list_sol_detaliat(Stream,[H|T]):- 
		afis_list_sol_detaliat(Stream,T), 
		H =.. [_, FC, Fapt],
		Fapt =.. [_, Av],
		Av =.. [_,Atr, Val], 
		scrie_solutie_in_fisier(Av, FC),
		scrie_scop(Stream,av(Atr, Val), FC).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End

afis_list_sol([H]):- H =.. [_, FC, Fapt],
					   Fapt =.. [_, Av],
					   Av =.. [_,Atr, Val], 
					   scrie_scop_scurt(av(Atr, Val), FC), nl.

afis_list_sol([H|T]):- afis_list_sol(T), 
					   H =.. [_, FC, Fapt],
					   Fapt =.. [_, Av],
					   Av =.. [_,Atr, Val], 
					   scrie_scop_scurt(av(Atr, Val), FC), nl.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Start
afis_list_sol(Stream, [H]):- H =.. [_, FC, Fapt],
					   Fapt =.. [_, Av],
					   Av =.. [_,Atr, Val], 
					   scrie_scop_scurt(Stream, av(Atr, Val), FC), 
					   nl(Stream), flush_output(Stream).

afis_list_sol(Stream, [H|T]):- afis_list_sol(Stream, T), 
					   H =.. [_, FC, Fapt],
					   Fapt =.. [_, Av],
					   Av =.. [_,Atr, Val], 
					   scrie_scop_scurt(Stream, av(Atr, Val), FC), nl(Stream),
					   flush_output(Stream).


%******************************************************************



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End
/*

afis_list_scop([H|T]):- 
	afis_list_scop(T),
	 H=..[_, FC, Fapt], 
	 Fapt=..[_, Av], 
	 Av=..[_, Atr, Val], nl, 
	 scrie_solutie_in_fisier(Av,FC), 
	 scrie_scop(av(Atr,Val),FC), nl.
afis_list_scop([]).

afis_list_scop([H]):- H =.. [_, FC, Fapt],
					   Fapt =.. [_, Av],
					   Av =.. [_,Atr, Val], 
					   scrie_scop(av(Atr, Val), FC), nl.

afis_list_scop([H|T]):- afis_list_scop(T), 
					   H =.. [_, FC, Fapt],
					   Fapt =.. [_, Av],
					   Av =.. [_,Atr, Val], 
					   scrie_scop(av(Atr, Val), FC), nl.*/

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Start
/*afis_list_scop(Stream, [H|T]):- 
	write('afis_list_scop'),nl,
	afis_list_scop(Stream, T),
	 H=..[_, FC, Fapt], 
	 Fapt=..[_, Av], 
	 Av=..[_, Atr, Val], nl, 
	 scrie_solutie_in_fisier(Av,FC), 
	 scrie_scop(Stream, av(Atr,Val),FC), nl.

afis_list_scop(_, []).
*/
/*
afis_list_scop(Stream, [H]):- 
					   H =.. [_, FC, Fapt],
					   Fapt =.. [_, Av],
					   Av =.. [_,Atr, Val],
					   scrie_solutie_in_fisier(Av, FC), 
					   scrie_scop(Stream, av(Atr, Val), FC). 
					   %nl(Stream), flush_output(Stream).

afis_list_scop(Stream, [H|T]):- 
					   afis_list_scop(Stream, T), 
					   H =.. [_, FC, Fapt],
					   Fapt =.. [_, Av],
					   Av =.. [_,Atr, Val], 
					   scrie_scop(Stream, av(Atr, Val), FC). 
					   %nl(Stream), flush_output(Stream).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End

/*afis_list_scop_scurt([H]):- 
	H=..[_, FC, Fapt], 
	Fapt=..[_, Av], 
	Av=..[_, Atr, Val], nl, 
	scrie_scop_scurt(av(Atr,Val),FC), nl.
	
afis_list_scop_scurt([H|T]):- 
	afis_list_scop_scurt(T), 
	H=..[_, FC, Fapt], 
	Fapt=..[_, Av], 
	Av=..[_, Atr, Val], nl, 
	scrie_scop_scurt(av(Atr,Val),FC), nl. 
afis_list_scop_scurt([]).
*/
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Start
/*afis_list_scop_scurt(Stream, [H]):- 
		H=..[_, FC, Fapt], 
		Fapt=..[_, Av], 
		Av=..[_, Atr, Val], nl, 
		scrie_scop_meniu_sumar(Stream, av(Atr,Val),FC), nl. 

afis_list_scop_scurt(Stream, [H|T]):- 
		afis_list_scop_scurt(Stream, T), 
		H=..[_, FC, Fapt], 
		Fapt=..[_, Av], 
		Av=..[_, Atr, Val], nl, 
		scrie_scop_meniu_sumar(Stream, av(Atr,Val),FC), nl. 
afis_list_scop_scurt(_,[]).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End
*/

%afis_list_scop(_, []).

afis_list_scop([H|T]):- 
	afis_list_scop(T),
	 H=..[_, FC, Fapt], 
	 Fapt=..[_, Av], 
	 Av=..[_, Atr, Val], nl, 
	 scrie_solutie_in_fisier(Av,FC), 
	 scrie_scop(av(Atr,Val),FC), nl.
afis_list_scop([]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Start
afis_list_scop(Stream, [H|T]):- 
	write('afis_list_scop'),nl,
	afis_list_scop(Stream, T),
	 H=..[_, FC, Fapt], 
	 Fapt=..[_, Av], 
	 Av=..[_, Atr, Val], nl, 
	 write('1'), write(H),nl,
	 scrie_solutie_in_fisier(Av,FC), 
	 write('2'), nl,
	 %format(Stream,"s(~p este ~p cu fc ~p)",[Atr,Val, FC]),
	 %nl(Stream), flush_output(Stream).
	scrie_scop(Stream, av(Atr,Val),FC), nl,
write('3'), nl.
afis_list_scop(_, []).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End


afis_list_scop_scurt([H|T]):- 
	afis_list_scop(T), 
	H=..[_, FC, Fapt], 
	Fapt=..[_, Av], 
	Av=..[_, Atr, Val], nl, 
	scrie_scop_scurt(av(Atr,Val),FC), nl. 
afis_list_scop_scurt([]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Start
afis_list_scop_scurt(Stream, [H|T]):- 
	afis_list_scop(Stream, T), 
	H=..[_, FC, Fapt], 
	Fapt=..[_, Av], 
	Av=..[_, Atr, Val], nl, 
	scrie_scop_meniu_sumar(Stream, av(Atr,Val),FC), nl. 
afis_list_scop_scurt(Stream, []).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End 

determina(Atr) :- realizare_scop(av(Atr,_),_,[scop(Atr)]),!.
determina(_).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Start
determina(Stream,Atr) :- realizare_scop(Stream,av(Atr,_),_,[scop(Atr)]),!.
determina(_,_).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End

afiseaza_scop(Atr) :-
	nl,fapt(av(Atr,Val),FC,_),
	FC >= 49,
	scrie_scop(av(Atr,Val),FC), nl,fail.

afiseaza_scop(_):-nl,nl.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Start
afiseaza_scop(Stream, Atr) :-
		nl, fapt(av(Atr,Val),FC,_),
		FC >= 49,
		format(Stream,"s(~p este ~p cu fc ~p)",[Atr,Val, FC]),
		nl(Stream),flush_output(Stream),fail.

afiseaza_scop(_,_):- write('Final!'),nl.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End

scrie_scop_scurt(av(Atr,Val),FC) :-
		transformare1(av(Atr,Val), X),
		scrie_lista(X),tab(2),
		write(' '),
		write('factorul de certitudine este '),
		FC1 is integer(FC),write(FC1), nl,
		solutie(_, _, _,  Desc),
		write('Descriere:'), nl, write(Desc), nl,!.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Start
scrie_scop_meniu_sumar(Stream, av(Atr,Val),FC) :-
		transformare(av(Atr,Val), X),
		FC1 is integer(FC),
		format(Stream,'s(~s)',[Val]), nl(Stream), flush_output(Stream).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End

scrie_scop(av(Atr,Val),FC) :-
		transformare1(av(Atr,Val), X),
		scrie_lista(X),tab(2),
		write(' '),
		write('factorul de certitudine este '),
		FC1 is integer(FC),write(FC1), nl,
		solutie(Val, Img, LProp,  Desc),
		write('Imagine: '), write(Img), nl,
		write('Descriere:'), nl, write(Desc), nl,
		write('Prop: '), afis_lprop(LProp),nl.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Start
scrie_scop(Stream, av(Atr,Val),FC) :-
	    %write('Scrie scop'), nl,
		transformare1(av(Atr,Val), X),
		write(X), nl,
		%scrie_lista(X),tab(2),
		%write(Stream, ' '),
		%write(Stream, 'factorul de certitudine este '),
		FC1 is integer(FC),write(Stream, FC1), nl(Stream),
		solutie(Val, Img, LProp,  Desc),
		write(solutie(Val, Img, LProp,  Desc)), nl, 
		LProp = [av('varsta', SolVarsta), av('tip', SolTip),av('beneficii',SolCrestere),av('pret',SolPret)],
		write(LProp), nl, 
		atom_concat('Varsta = ',SolVarsta, PropsAux),
		atom_concat(PropsAux,' Tip = ', PropsAux2),
		atom_concat(PropsAux2, SolTip, PropsAux3),
		atom_concat(PropsAux3,' Beneficii = ', PropsAux4),
		atom_concat(PropsAux4, SolCrestere, PropsAux5),

		Pret is integer(SolPret),
		number_chars(Pret,Chars),
		atom_chars(PRET,Chars),
		atom_concat(PropsAux5,' Pret = ', PropsAux6),
		atom_concat(PropsAux6,PRET, LPropp),
		write(LPropp), nl, 
		/*write(Stream, 'Imagine: '), write(Stream, Img), nl(Stream),
		write(Stream, 'Descriere:'), nl, write(Stream, Desc), nl(Stream),
		write(Stream, 'Prop: '), nl, write(Stream, LProp), nl(Stream),
		LProp = [av('varsta', SolVarsta), av('tip', SolTip),av('beneficii',SolCrestere),av('pret',SolPret)],
		atom_concat('Varsta = ',SolVarsta, PropsAux),
		atom_concat(PropsAux,' Tip = ', PropsAux2),
		atom_concat(PropsAux2, SolTip, PropsAux3),
		atom_concat(PropsAux3,' Beneficii = ', PropsAux4),
		atom_concat(PropsAux4, SolCrestere, PropsAux5),

		Pret is integer(SolPret),
		number_chars(Pret,Chars),
		atom_chars(PRET,Chars),
		atom_concat(PropsAux5,' Pret = ', PropsAux6),
		atom_concat(PropsAux6,PRET, LPropp),
		%write(Stream, 'AAAAAAAAAAAAAAAAA: '),
		%write(Stream,LPropp),
		%nl(Stream),*/
		format(Stream,'s(~s#~d#~s#~s#~s)',[Val,FC1, Img, Desc,LPropp]), 
		nl(Stream),
		flush_output(Stream).
	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End

afis_lprop([H|T]):- 
	H=..[_,Atr,Prop], 
	format('~s = ~s, ', [Atr, Prop]), 
	afis_lprop(T).

%afis_lprop([H]):- H=..[_,Atr,Prop], format('~s = ~s.', [Atr, Prop]).
afis_lprop([]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Start

afis_lprop(Stream, [H|T]):- 
	H=..[_,Atr,Prop], 
	format(Stream, '~s = ~s, ', [Atr, Prop]), 
	afis_lprop(Stream, T).
%afis_lprop(Stream, [H]):- H=..[_,Atr,Prop], format(Stream, '~s = ~s.', [Atr, Prop]).
afis_lprop(_,[]).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End
/*
realizare_scop(not Scop,Not_FC,Istorie):- 
	realizare_scop(Scop,FC,Istorie),
	Not_FC is - FC, !.
realizare_scop(Scop,FC,_):- fapt(Scop,FC,_), !.
realizare_scop(av(Atr,_),FC,_):- fapt(av(Atr,nu_conteaza),FC,_), !.
realizare_scop(not Scop,Not_FC,Istorie) :-
		realizare_scop(Scop,FC,Istorie),
		Scop = av(Atr,_),
		fapt(av(Atr,nu_conteaza),FC2,_),
		Not_FC is FC2, !.
realizare_scop(Scop,FC,Istorie):- 
	pot_interoga(Scop,Istorie),!,
	realizare_scop(Scop,FC,Istorie).
realizare_scop(Scop,FC_curent,Istorie):- fg(Scop,FC_curent,Istorie).*/

realizare_scop(av(Atr,_),FC,_) :-
	fapt(av(Atr,nu_conteaza),FC,_), !.
				
realizare_scop(not Scop,Not_FC,Istorie) :-
	realizare_scop(Scop,_,Istorie),
	Scop = av(Atr,_),
	fapt(av(Atr,nu_conteaza),FC2,_),
	Not_FC is FC2, !.
				
realizare_scop(not Scop,Not_FC,Istorie) :-
	realizare_scop(Scop,FC,Istorie),
	Not_FC is - FC, !.
				
realizare_scop(Scop,FC,_) :-
	fapt(Scop,FC,_), !.
			
realizare_scop(Scop,FC,Istorie) :-
	pot_interoga(Scop,Istorie),
	!,realizare_scop(Scop,FC,Istorie).
			
realizare_scop(Scop,FC_curent,Istorie):- fg(Scop,FC_curent,Istorie).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Start
/*
realizare_scop(Stream, not Scop,Not_FC,Istorie):- 
	realizare_scop(Stream, Scop,FC,Istorie),
	Not_FC is - FC, !.
realizare_scop(Stream, Scop,FC,_):- fapt(Scop,FC,_), !.
realizare_scop(Stream, av(Atr,_),FC,_):- fapt(av(Atr,nu_conteaza),FC,_), !.
realizare_scop(Stream, not Scop,Not_FC,Istorie) :-
	realizare_scop(Stream, Scop,FC,Istorie),
	Scop = av(Atr,_),
	fapt(av(Atr,nu_conteaza),FC2,_),
	Not_FC is FC2, !.
realizare_scop(Stream, Scop,FC,Istorie):- 
	pot_interoga(Stream, Scop,Istorie),!,
	realizare_scop(Stream, Scop,FC,Istorie).
realizare_scop(Stream, Scop,FC_curent,Istorie):- fg(Stream, Scop,FC_curent,Istorie).*/


realizare_scop(Stream, av(Atr,_),FC,_) :-
	fapt(av(Atr,nu_conteaza),FC,_), !.
							
realizare_scop(Stream, not Scop,Not_FC,Istorie) :-
	realizare_scop(Stream, Scop,_,Istorie),
	Scop = av(Atr,_),
	fapt(av(Atr,nu_conteaza),FC2,_),
	Not_FC is FC2, !.
							
realizare_scop(Stream, not Scop,Not_FC,Istorie) :-
	realizare_scop(Stream, Scop,FC,Istorie),
	Not_FC is - FC, !.
							
realizare_scop(Stream, Scop,FC,_) :-
	fapt(Scop,FC,_), !.
						
realizare_scop(Stream, Scop,FC,Istorie) :-
	pot_interoga(Stream, Scop,Istorie),
	!,realizare_scop(Stream, Scop,FC,Istorie).
						
realizare_scop(Stream, Scop,FC_curent,Istorie):- fg(Stream, Scop,FC_curent,Istorie).
				
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End

fg(Scop,FC_curent,Istorie) :-
	regula(N, premise(Lista), concluzie(Scop,FC)),
	demonstreaza(N,Lista,FC_premise,Istorie),
	ajusteaza(FC,FC_premise,FC_nou),
	actualizeaza(Scop,FC_nou,FC_curent,N),
	FC_curent == 100,!.
fg(Scop,FC,_) :- fapt(Scop,FC,_).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Start
fg(Stream, Scop,FC_curent,Istorie) :-
		regula(N, premise(Lista), concluzie(Scop,FC)),
		demonstreaza(Stream, N,Lista,FC_premise,Istorie),
		ajusteaza(FC,FC_premise,FC_nou),
		actualizeaza(Scop,FC_nou,FC_curent,N),
		FC_curent == 100,!.
fg(_,Scop, FC,_) :- fapt(Scop,FC,_).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End

pot_interoga(av(Atr,_),Istorie) :-
	not interogat(av(Atr,_)),
	interogabil(Atr,Optiuni,Mesaj),
	interogheaza(Atr,Mesaj,Optiuni,Istorie),nl,
	asserta( interogat(av(Atr,_)) ).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Start
pot_interoga(Stream, av(Atr,_),Istorie) :-
		not interogat(av(Atr,_)),
		interogabil(Atr,Optiuni,Mesaj),
		write('Optiunile inainte de interogheaza sunt '),
		write(Optiuni), nl, nl,
		interogheaza(Stream, Atr,Mesaj,Optiuni,Istorie),nl,write('S-a terminat interogheaza'),nl,
		asserta( interogat(av(Atr,_)) ).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End

cum([]) :- 
	write('Scop? '),nl,
	write('|:'),citeste_linie(Linie),nl,
	transformare1(Scop,Linie), cum(Scop).
cum(L) :- transformare1(Scop,L),nl, cum(Scop).
cum(not Scop) :- 
	fapt(Scop,FC,Reguli),
	lista_float_int(Reguli,Reguli1),
	FC < -49,transformare1(not Scop,PG),
	append(PG,[a,fost,derivat,cu, ajutorul, 'regulilor: '|Reguli1],LL),
	scrie_lista(LL),nl,afis_reguli(Reguli),fail.
cum(Scop) :-
	fapt(Scop,FC,Reguli),
	lista_float_int(Reguli,Reguli1),
	FC > 49,transformare1(Scop,PG),
	append(PG,[a,fost,derivat,cu, ajutorul, 'regulilor: '|Reguli1],LL),
	scrie_lista(LL),nl,afis_reguli(Reguli),
	fail.
cum(_).


afis_reguli([]).
afis_reguli([N|X]) :-
	afis_regula(N),
	premisele(N),
	afis_reguli(X).
afis_regula(N) :-
	regula(N, premise(Lista_premise),
	concluzie(Scop,FC)),NN is integer(N),
	scrie_lista(['#',NN]),
	scrie_lista(['pr:']),
	scrie_lista_premise(Lista_premise),
	scrie_lista(['c:']),
	FC1 is integer(FC),
	transformare(Scop, _,Scop_tr),
	append(['    '],Scop_tr,L1),
	append(L1,[FC1],LL),
	scrie_lista(LL),nl.


scrie_lista_premise([]).
scrie_lista_premise([H|T]) :-
	transformare(H,H_tr),
	tab(5),scrie_lista(H_tr),
	scrie_lista_premise(T).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Start

scrie_lista_premise(_,[]).
scrie_lista_premise(Stream, [H|T]) :-
	transformare(H,H_tr),
	tab(Stream, 5),scrie_lista(Stream, H_tr),
	scrie_lista_premise(Stream, T).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End

transformare1(av(A,V),[A,'este',V]).

transformare(av(A,da),[yes, ':', A]) :- !.
transformare(not av(A,da), [not,':',A]) :- !.
transformare(av(A,nu),[not,':', A]) :- !.
transformare(av(A,V),[A,'#',V]):-!.

transformare(av(A,da),_, [yes,':', A, 'fc#']):-!.
transformare(not av(A,da),_, [no,':',A, 'fc#']):-!.
transformare(av(A,V), _, [A,'#',V, 'fc#']).


premisele(N) :-
	regula(N, premise(Lista_premise), _),
	!, cum_premise(Lista_premise).

        
cum_premise([]).
cum_premise([Scop|X]) :-
	cum(Scop),
	cum_premise(X).

        
interogheaza(Atr,Mesaj,[da,nu],Istorie) :-
	!,write(Mesaj),nl, write('(da nu nu_stiu nu_conteaza)'),nl,
	de_la_utiliz(X,Istorie,[da,nu,nu_stiu,nu_conteaza]),
	det_val_fc(X,Val,FC),
	asserta( fapt(av(Atr,Val),FC,[utiliz]) ).

interogheaza(Atr,Mesaj,Optiuni,Istorie) :-
	write(Mesaj),nl,
	citeste_opt(VLista,Optiuni,Istorie),
	assert_fapt(Atr,VLista).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Start
interogheaza(Stream, Atr,Mesaj,[da,nu],Istorie) :- !,
		write(Stream, i(Mesaj)), nl(Stream), flush_output(Stream),
		write('\n Intrebare atr boolean\n'),
		citeste_opt(Stream,X,[da,nu],Istorie),
		det_val_fc(X,Val,FC),
		asserta( fapt(av(Atr,Val),FC,[utiliz]) ).

interogheaza(Stream, Atr,Mesaj,Optiuni,Istorie) :-
		write('\n Intrebare atr val multiple\n'),
		write(Stream,i(Mesaj)),nl(Stream),flush_output(Stream),
		citeste_opt(Stream,VLista,Optiuni,Istorie),
		assert_fapt(Atr,VLista).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End

citeste_opt(X,Optiuni,Istorie) :-
	append(['('],Optiuni,Opt1),
	append(Opt1,['nu_stiu nu_conteaza)'],Opt),
	scrie_lista(Opt),
	append(Optiuni,[nu_stiu ,nu_conteaza],Optiuni1),
	de_la_utiliz(X,Istorie,Optiuni1).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Start
citeste_opt(Stream,X,Optiuni,Istorie) :-
		append(Optiuni,[nu_stiu, nu_conteaza],Opt2),
		append(['('],Opt2,Opt1),
		append(Opt1,[')'],Opt),
		scrie_lista(Stream,Opt),
		de_la_utiliz(Stream,X,Istorie,Opt2).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End

de_la_utiliz(X,Istorie,Lista_opt) :-
	repeat,write(': '),citeste_linie(X),
	proceseaza_raspuns(X,Istorie,Lista_opt).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Start
de_la_utiliz(Stream,X,Istorie,Lista_opt) :-
		repeat,
			write('Astept raspuns\n'),
			citeste_linie(Stream,X),
			format('Am citit ~p din optiunile ~p\n',[X,Lista_opt]),
			proceseaza_raspuns(X,Istorie,Lista_opt), 
			write('Gata de la utiliz\n').
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End

proceseaza_raspuns([de_ce],Istorie,_) :- nl,afis_istorie(Istorie),!,fail.
proceseaza_raspuns([X],_,Lista_opt):- member(X,Lista_opt).
proceseaza_raspuns([X,fc,FC],_,Lista_opt):-member(X,Lista_opt),float(FC).

assert_fapt(Atr,[Val,fc,FC]) :-!,asserta( fapt(av(Atr,Val),FC,[utiliz]) ).
assert_fapt(Atr,[Val]) :- asserta( fapt(av(Atr,Val),100,[utiliz])).

det_val_fc([nu],da,-100).
det_val_fc([nu,FC],da,NFC) :- NFC is -FC.
det_val_fc([nu,fc,FC],da,NFC) :- NFC is -FC.
det_val_fc([Val,FC],Val,FC).
det_val_fc([Val,fc,FC],Val,FC).
det_val_fc([Val],Val,100).
        
afis_istorie([]) :- nl.
afis_istorie([scop(X)|T]) :- scrie_lista([scop,X]),!, afis_istorie(T).
afis_istorie([N|T]) :- afis_regula(N),!,afis_istorie(T).

demonstreaza(N,ListaPremise,Val_finala,Istorie) :-
	dem(ListaPremise,100,Val_finala,[N|Istorie]),!.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Start
demonstreaza(Stream, N,ListaPremise,Val_finala,Istorie) :-
	dem(Stream, ListaPremise,100,Val_finala,[N|Istorie]),!.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End


dem([],Val_finala,Val_finala,_).
dem([H|T],Val_actuala,Val_finala,Istorie) :-
	realizare_scop(H,FC,Istorie),
	Val_interm is min(Val_actuala,FC),
	Val_interm >= 49,
	dem(T,Val_interm,Val_finala,Istorie).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Start
dem(_, [],Val_finala,Val_finala,_).
dem(Stream, [H|T],Val_actuala,Val_finala,Istorie) :-
	realizare_scop(Stream, H,FC,Istorie),
	Val_interm is min(Val_actuala,FC),
	Val_interm >= 49,
	dem(Stream, T,Val_interm,Val_finala,Istorie).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End

actualizeaza(Scop,FC_nou,FC,RegulaN) :-
	fapt(Scop,FC_vechi,_),
	combina(FC_nou,FC_vechi,FC),
	retract( fapt(Scop,FC_vechi,Reguli_vechi) ),
	asserta( fapt(Scop,FC,[RegulaN | Reguli_vechi]) ),!.
actualizeaza(Scop,FC,FC,RegulaN) :- asserta( fapt(Scop,FC,[RegulaN]) ).

ajusteaza(FC1,FC2,FC) :- X is FC1 * FC2 / 100, FC is round(X).

combina(FC1,FC2,FC) :-
	FC1 >= 0,FC2 >= 0,
	X is FC2*(100 - FC1)/100 + FC1,
	FC is round(X).
combina(FC1,FC2,FC) :-
	FC1 < 0,FC2 < 0,
	X is - ( -FC1 -FC2 * (100 + FC1)/100),
	FC is round(X).
combina(FC1,FC2,FC) :-
	(FC1 < 0; FC2 < 0),
	(FC1 > 0; FC2 > 0),
	FCM1 is abs(FC1),FCM2 is abs(FC2),
	MFC is min(FCM1,FCM2),
	X is 100 * (FC1 + FC2) / (100 - MFC),
	FC is round(X).


incarca:-
	write('Introduceti regulilele: '),nl, write('|:'),read(FR),
	file_exists(FR),!,incarca_r(FR),
	write('Introduceti solutiile: '),nl, write('|:'),read(FS),
	file_exists(FS),!,incarca_s(FS).
incarca:- write('Nume incorect de fisier! '),nl,fail.


incarca_r(F) :-
	retractall(interogat(_)),retractall(fapt(_,_,_)),
	retractall(scop(_)),retractall(interogabil(_,_,_)),
	retractall(regula(_,_,_)),
	see(F),incarca_reguli,seen,!.

incarca_s(F) :-
		retractall(solutie(_,_,_,_)),
		see(F),incarca_solutii,seen,!.

incarca_reguli:- 
	repeat,
		citeste_propozitie(L),
		proceseaza_reguli(L),
		L == [end_of_file],nl.
incarca_solutii:- 
	repeat,
		citeste_propozitie(L),
		proceseaza_solutii(L),
		L == [end_of_file],nl.

proceseaza_reguli([end_of_file]):-!.
proceseaza_reguli(L) :- %write('L= '), write(L), nl,
	trad(R,L,[]),write('R= '), write(R), nl, assertz(R), !.

proceseaza_solutii([end_of_file]):-!.
proceseaza_solutii(L) :- %write('L= '), write(L),
	trad(R,L,[]),write('S= '), write(R), nl, assertz(R), !.

trad(scop(X)) --> [scop,'#',X].
trad(interogabil(Atr,M,P)) --> ['#',i, ':',Atr], lista_optiuni(M), afiseaza(Atr,P).
trad(regula(N,premise(Daca),concluzie(Atunci,F))) --> identificator(N), daca(Daca), atunci(Atunci,F).
trad(solutie(Sol, Img, LProp,  Desc)) --> [solutie, '[', Sol, ']', 
										  imagine, '[', Img, ']', 
										  prop,':'], lista_lprop(LProp), 
										  [descriere, '[', Desc,']'].

trad('Eroare la parsare'-L,L,_).

lista_lprop([av(Atr,Val)|T]) --> ['[', Atr, ',', Val,']'], lista_lprop(T).
lista_lprop([av(Atr,Val)]) --> ['[', Atr, ',', Val,']'].

lista_optiuni(M) --> [val,':'],lista_de_optiuni(M).

lista_de_optiuni([Element|T]) --> ['#',Element], lista_de_optiuni(T).
lista_de_optiuni([Element]) -->  ['#',Element].


afiseaza(_,P) -->  [text_intrebare,'#',P].
afiseaza(P,P) -->  [].

identificator(N) --> ['#',N].


daca(Daca) --> [pr,':'],lista_premise(Daca).

%propoz parseaza atribut variabila
lista_premise([Daca]) --> propoz(Daca),[c,':'].
lista_premise([Prima|Celalalte]) --> propoz(Prima),[ ],lista_premise(Celalalte).
%lista_premise([Prima|Celalalte]) --> propoz(Prima),[','],lista_premise(Celalalte).


atunci(Atunci,FC) --> propoz(Atunci),[fc,'#'],[FC].
atunci(Atunci,100) --> propoz(Atunci).


propoz(not av(Atr,da)) --> ['not',':', Atr].
propoz(av(Atr,da)) --> ['yes',':', Atr].
propoz(av(Atr,Val)) --> [Atr,'#',Val].



citeste_linie([Cuv|Lista_cuv]) :-
	get_code(Car), 							% citest pana gasesc ceva separat de litera( spatiu ) 
	citeste_cuvant(Car, Cuv, Car1),         %car1 este caracterul care delimiteaza token-ul... 
	rest_cuvinte_linie(Car1, Lista_cuv).    % restul cuvintelor de pe linie

rest_cuvinte_linie(-1, []):-!.    
	rest_cuvinte_linie(Car,[]) :-(Car==13;Car==10), !. % daca dam de enter se termina lista
rest_cuvinte_linie(Car,[Cuv1|Lista_cuv]) :-  	   % forma recursiva
	citeste_cuvant(Car,Cuv1,Car1),      		   % citeste de la car pana la car1
	rest_cuvinte_linie(Car1,Lista_cuv).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%STREAM%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Start
citeste_linie(Stream, [Cuv|Lista_cuv]) :-
		get_code(Stream, Car), 							% citest pana gasesc ceva separat de litera( spatiu ) 
		citeste_cuvant(Stream, Car, Cuv, Car1),         %car1 este caracterul care delimiteaza token-ul... 
		rest_cuvinte_linie(Stream, Car1, Lista_cuv).    % restul cuvintelor de pe linie
	
rest_cuvinte_linie(_,-1, []):-!.    
	rest_cuvinte_linie(_,Car,[]) :-(Car==13;Car==10), !.          % daca dam de enter se termina lista
rest_cuvinte_linie(Stream, Car,[Cuv1|Lista_cuv]) :-  	   % forma recursiva
	citeste_cuvant(Stream, Car,Cuv1,Car1),      		   % citeste de la car pana la car1
	rest_cuvinte_linie(Stream, Car1,Lista_cuv).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End     

% -1 este codul ASCII pt EOF


citeste_propozitie([Cuv|Lista_cuv]) :-
	get_code(Car),citeste_cuvant(Car, Cuv, Car1), 
	rest_cuvinte_propozitie(Car1, Lista_cuv).
 
     
rest_cuvinte_propozitie(-1, []):-!.
rest_cuvinte_propozitie(Car,[]) :-Car==46, !.
rest_cuvinte_propozitie(Car,[Cuv1|Lista_cuv]) :- 
	citeste_cuvant(Car,Cuv1,Car1),      
	rest_cuvinte_propozitie(Car1,Lista_cuv).


citeste_cuvant(-1,end_of_file,-1):-!.
citeste_cuvant(Caracter,Cuvant,Caracter1) :-   
	caracter_cuvant(Caracter),!, 						%daca e caracter special(de sine statator)
	name(Cuvant, [Caracter]),get_code(Caracter1).       % din cod ascii in caracter, si dupa mai citim un caracter cu get_code
citeste_cuvant(Caracter, Numar, Caracter1) :-
	caracter_numar(Caracter),!,
	citeste_tot_numarul(Caracter, Numar, Caracter1).
 

citeste_tot_numarul(Caracter,Numar,Caracter1):-     % determina numarul 
	determina_lista(Lista1,Caracter1),
	append([Caracter],Lista1,Lista),
	transforma_lista_numar(Lista,Numar).


determina_lista(Lista,Caracter1):-    % citeste pana cand s a terminat numarul 
	get_code(Caracter), 
	(caracter_numar(Caracter),
	determina_lista(Lista1,Caracter1),
	append([Caracter],Lista1,Lista); 
	\+(caracter_numar(Caracter)),
	Lista=[],Caracter1=Caracter).
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%STREAM%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Start
rest_cuvinte_propozitie(_,-1, []):-!.
rest_cuvinte_propozitie(_, Car,[]) :-Car==46, !.
rest_cuvinte_propozitie(Stream, Car,[Cuv1|Lista_cuv]) :- 
	citeste_cuvant(Stream, Car,Cuv1,Car1),      
	rest_cuvinte_propozitie(Stream, Car1,Lista_cuv).

citeste_cuvant(_, -1,end_of_file,-1):-!.
citeste_cuvant(Stream, Caracter,Cuvant,Caracter1) :-   
	caracter_cuvant(Caracter),!, 						        %daca e caracter special(de sine statator)
	name(Cuvant, [Caracter]),get_code(Stream, Caracter1).       % din cod ascii in caracter, si dupa mai citim un caracter cu get_code
citeste_cuvant(Stream, Caracter, Numar, Caracter1) :-
	caracter_numar(Caracter),!,
	citeste_tot_numarul(Stream, Caracter, Numar, Caracter1).
 

citeste_tot_numarul(Stream, Caracter,Numar,Caracter1):-     % determina numarul 
	determina_lista(Stream, Lista1,Caracter1),
	append([Caracter],Lista1,Lista),
	transforma_lista_numar(Lista,Numar).


determina_lista(Stream, Lista,Caracter1):-    % citeste pana cand s a terminat numarul 
	get_code(Stream, Caracter), 
	(caracter_numar(Caracter),
	determina_lista(Stream, Lista1,Caracter1),
	append([Caracter],Lista1,Lista); 
	\+(caracter_numar(Caracter)),
	Lista=[],Caracter1=Caracter).
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%STREAM%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End

transforma_lista_numar([],0). % daca e vida returneaza 0
transforma_lista_numar([H|T],N):-   % daca e o lista de coduri ascii
	transforma_lista_numar(T,NN), 
	lungime(T,L), Aux is exp(10,L),
	HH is H-48,N is HH*Aux+NN.

lungime([],0).
lungime([_|T],L):- lungime(T,L1), L is L1+1.

% 39 este codul ASCII pt

citeste_cuvant(Caracter,Cuvant,Caracter1) :-
	Caracter==39,!,
	pana_la_urmatorul_apostrof(Lista_caractere),
	L=[Caracter|Lista_caractere],
	name(Cuvant, L),get_code(Caracter1).
        

pana_la_urmatorul_apostrof(Lista_caractere):-
	get_code(Caracter),
	(Caracter == 39,Lista_caractere=[Caracter];
	Caracter\==39,
	pana_la_urmatorul_apostrof(Lista_caractere1),
	Lista_caractere=[Caracter|Lista_caractere1]).


citeste_cuvant(Caracter,Cuvant,Caracter1) :-          % ultima clauza a lui citeste cuvant
	caractere_in_interiorul_unui_cuvant(Caracter),!,              
	((Caracter>64,Caracter<91),!,
	Caracter_modificat is Caracter+32;
	Caracter_modificat is Caracter),                              
	citeste_intreg_cuvantul(Caractere,Caracter1),
	name(Cuvant,[Caracter_modificat|Caractere]).
        

citeste_intreg_cuvantul(Lista_Caractere,Caracter1) :-
	get_code(Caracter),
	(caractere_in_interiorul_unui_cuvant(Caracter),
	((Caracter>64,Caracter<91),!, 
	Caracter_modificat is Caracter+32;
	Caracter_modificat is Caracter),
	citeste_intreg_cuvantul(Lista_Caractere1, Caracter1),
	Lista_Caractere=[Caracter_modificat|Lista_Caractere1]; 
	\+(caractere_in_interiorul_unui_cuvant(Caracter)),
	Lista_Caractere=[], Caracter1=Caracter).


citeste_cuvant(_,Cuvant,Caracter1) :-                % daca e spatiu sare la urmatorul caracter
	get_code(Caracter),       
	citeste_cuvant(Caracter,Cuvant,Caracter1).
 

caracter_cuvant(C):-member(C,[42,44,59,58,63,33,46,41,40,35,91,93]). % caracterele speciale care nu apar in lista , ne ducem la 460

% am specificat codurile ASCII pentru , ; : ? ! . ) (
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%STREAM%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Start

citeste_cuvant(Stream, Caracter,Cuvant,Caracter1) :-
		Caracter==39,!,
		pana_la_urmatorul_apostrof(Stream, Lista_caractere),
		L=[Caracter|Lista_caractere],
		name(Cuvant, L),get_code(Stream, Caracter1).
			
	
	pana_la_urmatorul_apostrof(Stream, Lista_caractere):-
		get_code(Stream, Caracter),
		(Caracter == 39,Lista_caractere=[Caracter];
		Caracter\==39,
		pana_la_urmatorul_apostrof(Stream, Lista_caractere1),
		Lista_caractere=[Caracter|Lista_caractere1]).
	
	
	citeste_cuvant(Stream, Caracter,Cuvant,Caracter1) :-          % ultima clauza a lui citeste cuvant
		caractere_in_interiorul_unui_cuvant(Caracter),!,              
		((Caracter>64,Caracter<91),!,
		Caracter_modificat is Caracter+32;
		Caracter_modificat is Caracter),                              
		citeste_intreg_cuvantul(Stream, Caractere,Caracter1),
		name(Cuvant,[Caracter_modificat|Caractere]).
			
	
	citeste_intreg_cuvantul(Stream, Lista_Caractere,Caracter1) :-
		get_code(Stream, Caracter),
		(caractere_in_interiorul_unui_cuvant(Caracter),
		((Caracter>64,Caracter<91),!, 
		Caracter_modificat is Caracter+32;
		Caracter_modificat is Caracter),
		citeste_intreg_cuvantul(Stream, Lista_Caractere1, Caracter1),
		Lista_Caractere=[Caracter_modificat|Lista_Caractere1]; \+(caractere_in_interiorul_unui_cuvant(Caracter)),
		Lista_Caractere=[], Caracter1=Caracter).
	
	
	citeste_cuvant(Stream,_,Cuvant,Caracter1) :-                % daca e spatiu sare la urmatorul caracter
		get_code(Stream, Caracter),       
		citeste_cuvant(Stream, Caracter,Cuvant,Caracter1).
	 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%STREAM%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End
caractere_in_interiorul_unui_cuvant(C):-   % se duce de la linia 516
	C>64,C<91;C>47,C<58;
	C==45;C==95;C>96,C<123.

caracter_numar(C):-C<58,C>=48.   % daca e cifra , intre 0 si 9 .

%demonstratia_pentru(numesol[factor-cert=fc]).txt
scrie_solutie_in_fisier(Av, FC):- nl, nl,
								  Av=..[_,_,Val],
								  telling(Output_current),
								  atom_concat('demonstratii/demonstratie_pentru(',Val,Aux1),
								  atom_concat(Aux1, '[factor-cert=', Aux2),
								  number_chars(FC, Y), 
								  atom_chars(Factor, Y),
								  atom_concat(Aux2, Factor, Aux3),
								  atom_concat(Aux3, ']).txt', Fisier),
								  tell(Fisier), 
								  cum(Av), 
								  told,
								  tell(Output_current).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% CONEXIUNE INTERFATA
inceput:-format('Salutare\n',[]),	flush_output,
				prolog_flag(argv, [PortSocket|_]), %preiau numarul portului, dat ca argument cu -a
												   %portul este atom, nu constanta numerica, asa ca trebuie sa il convertim la numar
				atom_chars(PortSocket,LCifre),
				number_chars(Port,LCifre),%transforma lista de cifre in numarul din 
				socket_client_open(localhost: Port, Stream, [type(text)]),
				proceseaza_text_primit(Stream,0).

				
proceseaza_text_primit(Stream,C):-
				write(inainte_de_citire),nl,
				read(Stream,CevaCitit),
				write(dupa_citire),nl,
				write(CevaCitit),nl,
				proceseaza_termen_citit(Stream,CevaCitit,C).
				
proceseaza_termen_citit(Stream,director(D),C):- %pentru a seta directorul curent
				format(Stream,'Locatia curenta de lucru s-a deplasat la adresa ~p.',[D]),
				format('Locatia curenta de lucru s-a deplasat la adresa ~p',[D]),
				X=current_directory(_,D),
				write(X),
				call(X),
				nl(Stream),
				flush_output(Stream),
				C1 is C+1,
				proceseaza_text_primit(Stream,C1).				
								
proceseaza_termen_citit(Stream, incarca(X),C):-
				write(Stream,'Se incearca incarcarea fisierului\n'),
				flush_output(Stream),
				incarca_r(X),
				C1 is C+1,
				proceseaza_text_primit(Stream,C1).
				
proceseaza_termen_citit(Stream, incarca_sol(X),C):-
				write(Stream,'Se incearca incarcarea fisierului\n'),
				flush_output(Stream),
				incarca_s(X),
				C1 is C+1,
				proceseaza_text_primit(Stream,C1).
				
proceseaza_termen_citit(Stream, comanda(consulta),C):-
				write(Stream,'Se incepe consultarea'), nl(Stream), flush_output(Stream),
				flush_output(Stream),
				scopuri_princ(Stream),
				C1 is C+1,
				proceseaza_text_primit(Stream,C1).
				
proceseaza_termen_citit(Stream, comanda(reinitiaza),C):-
				write(Stream, 'Se incepe reinitializarea'), nl(Stream), flush_output(Stream),
				executa(Stream, [reinitiaza]), 
				C1 is C+1,
				proceseaza_text_primit(Stream,C1).


executa(Stream, [reinitiaza]) :- write('Am reusit sa reinitializez'),nl,
	retractall(interogat(_)), retractall(fapt(_,_,_)),!.		

proceseaza_termen_citit(Stream, comanda(afisare_fapte),C):-
				write(Stream, 'Se incepe afisarea faptelor'), nl(Stream), flush_output(Stream),
				executa(Stream, [afisare_fapte]), 
				C1 is C+1,
				proceseaza_text_primit(Stream,C1).					
						
				
proceseaza_termen_citit(Stream, X, _):- % cand vrem sa-i spunem "Pa"
				(X == end_of_file ; X == exit),
				write(gata),nl,
				close(Stream).

proceseaza_termen_citit(Stream, Altceva,C):-
				write(Stream,'nu inteleg ce vrei sa spui: '),write(Stream,Altceva),nl(Stream),
				flush_output(Stream),
				C1 is C+1,
				proceseaza_text_primit(Stream,C1).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% END -> CONEXIUNE INTERFATA
