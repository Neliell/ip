﻿solutie ['Royal Canin-Kitten']
imagine ['imagini\\rc_kitten.jpg']
prop:
[varsta, junior]
[tip, uscata]
[beneficii, stimulare_crestere]
[pret, 25]
descriere ['In perioada de crestere, aparatul digestiv al pisoiului este imatur si se dezvolta gradual in cateva saptamani. Combinatia exclusiva de nutrimente ajuta la sustinerea aparatului digestiv al pisoiului prin asigurarea unei digestii optime si un scaun corect.'].



solutie ['Royal Canin-Kitten Sterilised']
imagine ['imagini\\rc_sterilised_kitten.jpg']
prop:
[varsta, junior]
[tip, uscata]
[beneficii, sistem_urinar]
[pret, 29]
descriere ['Hrana echilibrata si completa, special adaptata nevoilor nutritionale ale pisoilor sterilizati, ce au varsta cuprinsa intre 6 si 12 luni si se afla in cadrul celei de-a doua etape de crestere.'].



solutie ['Professional Food']
imagine ['imagini\\professional_food.jpg']
prop:
[varsta, junior]
[tip, uscata]
[beneficii, hrana_sanatoasa]
[pret, 12]
descriere ['Hrana uscata premium, pentru pisici junior, cu pui si peste. Nu contine adaos de zahar, aditivi de gust, coloranti.'].



solutie ['Royal Canin-Sterilised']
imagine ['imagini\\rc_sterilised.jpg']
prop:
[varsta, adult]
[tip, uscata]
[beneficii, sistem_urinar]
[pret, 29]
descriere ['Hrana echilibrata si completa, special adaptata nevoilor nutritionale ale pisicilor adulte sterilizate.'].



solutie ['Carnilove-Sterilised']
imagine ['imagini\\carnilove.jpg']
prop:
[varsta, adult]
[tip, uscata]
[beneficii, sistem_urinar]
[pret, 45]
descriere ['Hrana uscata superpremium pentru pisici sterilizate, cu mix de carne. Un aliment holistic echilibrat, care va satisface dorinta innascuta a pisicii pentru carne. Contine carne de origine salbatica, fructe de padure, legume si plante aromatice.'].



solutie ['Yarrah-Mancare BIO uscata']
imagine ['imagini\\yarrah.jpg']
prop:
[varsta, 'junior/adult/senior']
[tip, uscata]
[beneficii, 'sistem_urinar/sistem_osos']
[pret, 52]
descriere ['Nu contine aditivi, potentiatori de gust sau miros sau conservanti artificiali. Ingredientele provin din agricultura certificata BIO, fara organisme modificate genetic (OMG/GMO), fara pesticide, ierbicide, antibiotice, stimulenti de crestere etc. Hipoalergenica. FARA CEREALE. Nu contine adaos de zahar.'].



solutie ['Purina Cat Chow-Urinary']
imagine ['imagini\\purina.jpg']
prop:
[varsta, 'adult/senior']
[tip, uscata]
[beneficii, sistem_urinar]
[pret, 12]
descriere ['Hrana premium pentru pisici cu probleme urinare. Cat Chow Urinary Tract ajuta la mentinerea sanatatii sistemului urinar asigurand un nivel optim al ph-ului urinar.'].



solutie ['Royal Canin-Ageing 12+']
imagine ['imagini\\rc_ageing.jpg']
prop:
[varsta, senior]
[tip, uscata]
[beneficii, 'sistem_osos/sistem_urinar']
[pret, 30]
descriere ['Hrana  completa pentru pisici cu varsta de peste 12 ani, cu dintii si gingiile sensibile si un apetit scazut.'].



solutie ['Royal Canin(plic)']
imagine ['imagini\\rc_plic.jpg']
prop:
[varsta, 'adult/senior']
[tip, umeda]
[beneficii, sistem_urinar]
[pret, 3]
descriere ['Hrana completa pentru pisicile adulte. Ajuta la mentinerea sanatatii sistemului urinar, ajuta la mentinerea unei greutati ideale si are un profil nutritional preferat instinctiv.'].



solutie ['Royal Canin-Kitten(plic)']
imagine ['imagini\\rc_kitten_plic.jpg']
prop:
[varsta, junior]
[tip, umeda]
[beneficii, sistem_urinar]
[pret, 3]
descriere ['Bucatele de carne pentru pisoii aflati in perioada cresterii, cu varsta cuprinsa intre 4 si 12 luni. Hrana umeda Royal Canin Kitten Instinctive este potrivita pentru mentinerea sanatatii sistemului urinar.'].



solutie ['Royal Canin(conserva)']
imagine ['imagini\\rc_conserva.jpg']
prop:
[varsta, 'adult/senior']
[tip, umeda]
[beneficii, sistem_urinar]
[pret, 5]
descriere ['Hrana completa pentru pisicile adulte. Ajuta la mentinerea sanatatii sistemului urinar, ajuta la mentinerea unei greutati ideale si are un profil nutritional preferat instinctiv.'].



solutie ['Royal Canin-Kitten(conserva)']
imagine ['imagini\\rc_kitten_conserva.jpg']
prop:
[varsta, junior]
[tip, umeda]
[beneficii, sistem_urinar]
[pret, 5]
descriere ['Bucatele de carne pentru pisoii aflati in perioada cresterii, cu varsta cuprinsa intre 4 si 12 luni. Hrana umeda Royal Canin Kitten Instinctive este potrivita pentru mentinerea sanatatii sistemului urinar.'].

solutie ['Yarrah(conserva)']
imagine ['imagini\\yarrah_umed.jpg']
prop:
[varsta, 'adult/senior']
[tip, umeda]
[beneficii, sistem_urinar]
[pret, 12]
descriere ['Hrana completa premium pentru pisicile adulte.'].