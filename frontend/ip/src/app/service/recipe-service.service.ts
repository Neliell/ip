import { Injectable } from "@angular/core";
import { Headers, Http, RequestOptions, Response } from "@angular/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { catchError } from "rxjs/operators";
import { throwError } from "rxjs";
import { HttpErrorResponse } from "@angular/common/http";
import { Recipe } from "../model/recipe";

@Injectable({
  providedIn: "root"
})
export class RecipeServiceService {
  private baseUrl: String = "http://localhost:9092/recipe";
  private headers = new Headers({ "Content-Type": "application/json" });
  private options = new RequestOptions({ headers: this.headers });

  constructor(private _http: Http) {}

  public getAllRecipes() {
    return this._http
      .get(this.baseUrl + "/all", this.options)
      .pipe(map((response: Response) => response.json()));
  }
  // public postRecipe(recipe: Recipe) {
  //   console.log(' Recipes in service = ' + recipe);
  //   const body = JSON.stringify(recipe);
  //   console.log('JSON Recipes = ' + body);
  //   console.log('Base Url = ' + this.baseUrl);
  //   return this._http.post(this.baseUrl + "/send", body, this.options);
  // }

  errorHandler(error: Response) {
    return throwError(error || "Server error");
  }
}
