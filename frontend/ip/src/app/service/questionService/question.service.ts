import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { catchError } from 'rxjs/internal/operators/catchError';


@Injectable()
export class QuestionService {

  private baseUrl: string = 'http://localhost:9092/form';
  private headers = new Headers({ 'Content-Type': 'application/json' });
  private options = new RequestOptions({ headers: this.headers });

  constructor(private _http: Http) { }

  getQuestion() {
    return this._http.get(this.baseUrl + '/output', this.options).pipe(
      map((response: Response) => response.json()));
  }

  errorHandler(error: Response) {
    return Observable.throw(error || 'Server error');
  }

}
