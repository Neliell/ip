import {Component, OnInit} from '@angular/core';
import {Question} from "../Question";
import {QuestionService} from "../../service/questionService/question.service";

@Component({
  selector: 'app-listQuestion',
  templateUrl: './listQuestion.component.html',
  styleUrls: ['./listQuestion.component.css']
})
export class ListQuestionComponent implements OnInit {

  buttonIsClicked: boolean;
  private Question: Question[];

  constructor(private questionService: QuestionService) {

  }

  ngOnInit() {
    this.questionService.getQuestion().subscribe((question) => {
      console.log("intrebarea curenta");
      console.log(question);
      this.Question= question;
    }, (error) => {
      console.log(error);
    });
  }

}

