export class Recipe {
  nume_reteta: String;
  tip_bucatarie: String;
  tip_masa: String;
  descriere_reteta: String;
  imagine_reteta: String;
  ingrediente_reteta: String;
  mod_preparare: String;
  timp_preparare: String;
  grad_dificultate: String;
  numar_portii: String;
  aprecieri: Number;

  constructor(
    nume_reteta: String,
    tip_bucatarie: String,
    tip_masa: String,
    descriere_reteta: String,
    imagine_reteta: String,
    ingrediente_reteta: String,
    mod_preparare: String,
    timp_preparare: String,
    grad_dificultate: String,
    numar_portii: String,
    aprecieri: Number
  ) {
    this.nume_reteta = nume_reteta;
    this.tip_bucatarie = tip_bucatarie;
    this.tip_masa = tip_masa;
    this.descriere_reteta = descriere_reteta;
    this.imagine_reteta = imagine_reteta;
    this.ingrediente_reteta = ingrediente_reteta;
    this.mod_preparare = mod_preparare;
    this.timp_preparare = timp_preparare;
    this.grad_dificultate = grad_dificultate;
    this.numar_portii = numar_portii;
    this.aprecieri = aprecieri;
  }
}
