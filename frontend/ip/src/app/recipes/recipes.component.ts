import { Component, OnInit } from "@angular/core";
import { Recipe } from "../model/recipe";
import { RecipeServiceService } from "../service/recipe-service.service";

@Component({
  selector: "app-recipes",
  templateUrl: "./recipes.component.html",
  styleUrls: ["./recipes.component.css"]
})
export class RecipesComponent implements OnInit {
  recipes: Recipe[] = [];

  constructor(private _recipeService: RecipeServiceService) {}

  ngOnInit() {
    this._recipeService.getAllRecipes().subscribe(
      (recipes: Recipe[]) => {
        console.log(recipes);
        for (let i = 0; i < recipes.length; i++) {
          const recipe = recipes[i];
          this.recipes[i] = new Recipe(
            recipe.nume_reteta,
            recipe.tip_bucatarie,
            recipe.tip_masa,
            recipe.descriere_reteta,
            recipe.imagine_reteta,
            recipe.ingrediente_reteta,
            recipe.mod_preparare,
            recipe.timp_preparare,
            recipe.grad_dificultate,
            recipe.numar_portii,
            recipe.aprecieri
          );
        }
        console.log(this.recipes);
      },
      error => {
        console.log(error);
      }
    );
  }
}
