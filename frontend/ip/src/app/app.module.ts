import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponent } from "./components/home/home.component";
import { ListQuestionComponent } from "./components/listQuestion/listQuestion.component";
import { QuestionService } from "./service/questionService/question.service";
import { RouterModule, Routes } from "@angular/router";
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatCheckboxModule } from '@angular/material';
import { MatSidenavModule } from '@angular/material';
import { MatToolbarModule, MatIconModule, MatListModule, MatGridListModule, MatCardModule, MatMenuModule, MatTableModule, MatPaginatorModule, MatSortModule } from '@angular/material';
import { LayoutModule } from '@angular/cdk/layout';
import { NavigationComponent } from './navigation/navigation.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TableComponent } from './table/table.component';
import { RecipesComponent } from './recipes/recipes.component';
import { FlexLayoutModule } from '@angular/flex-layout';


const appRoutes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'listQuestion', component: ListQuestionComponent },
  { path: 'recipes', component: RecipesComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ListQuestionComponent,
    NavigationComponent,
    DashboardComponent,
    TableComponent,
    RecipesComponent,
  ],
  imports: [
    BrowserModule,
    FlexLayoutModule,
    NoopAnimationsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    HttpModule,
    MatButtonModule,
    MatCheckboxModule,
    MatSidenavModule,
    MatToolbarModule,
    RouterModule.forRoot(
      appRoutes
    ),
    LayoutModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule
  ],
  providers: [QuestionService],
  bootstrap: [AppComponent]
})

export class AppModule { }
