##Team Members : 

**Bragadireanu Ruxandra (452)**
**Bou Elena-Mirela (454)**
**Diaconu Alexandra (454) **
**Mitocaru Andreea (454)**
**Oprișan Anca-Cristina(454)**

## How to work with git: 

**1. Clone the project to your desired location  (Souce->Clone-> paste "git clone https://"  in git bash)**

**2. Go to 'Branches' tab and click 'Create Branch'=> type: feature/ and from branch: master**

**3. Open git bash in the directory where you cloned you project**

**4. git status  (to make sure git is present)** 

**5. git pull **

**6. git checkout <name_of_branch>**

**7. git branch (should show the name of the branch and not master)**

**8. Now you are on your branch and you can start working on your item **

After you are done with you changes:

**9. git status (to see what unversioned files you have)**

**10. git add .(to add all you files for versioning. You can add only certain files is you don't want all of them)**

**11. git commit -m "Put a meaningful message here"**

**12. git push**

**13. Now you can create a pull request: Go to pull requests, 'Create Pull Request' and select your branch**

**14. Add everyone from the team as a reviewer**

**15. Wait for the team to do a code review**

**16. Merge the code into master**

##Now you can celebrate :blush:


